#ifndef __MEDIC_QUEUE
#define __MEDIC_QUEUE

#define QUEUE_DATA_ITEM_SIZE (NRF_SDH_BLE_GATT_MAX_MTU_SIZE)

#define QUEUE_RX_MAX_LEN 8
#define QUEUE_TX_MAX_LEN (QUEUE_RX_MAX_LEN)

#define QUEUE_RX_DATA_SIZE (QUEUE_DATA_ITEM_SIZE * QUEUE_RX_MAX_LEN)
#define QUEUE_TX_DATA_SIZE (QUEUE_DATA_ITEM_SIZE * QUEUE_TX_MAX_LEN)

typedef struct 
{
	uint8_t *pItem;
	uint32_t ItemSize;
}BleQueueItemTypeDef;

typedef struct 
{
	QueueHandle_t RxQueue;
	QueueHandle_t TxQueue;
	ble_medic_t *pMedicInstance;
}BleTransactDataTypeDef;

typedef int (*QueueItemHandlerTypeDef) (uint8_t * pItem, uint32_t itemLen);

void InitOs(void);
BaseType_t PutDataToRxQueue(const uint8_t *data,uint16_t len);
BaseType_t PutDataToTxQueue(const uint8_t *data,uint16_t len);
BaseType_t GetDataFromRxQueue(QueueItemHandlerTypeDef itemHandleCallBack);
BaseType_t GetDataFromTxQueue(QueueItemHandlerTypeDef itemHandleCallBack);
#endif
