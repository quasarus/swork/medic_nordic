#ifndef __MEDIC_ADC
#define __MEDIC_ADC

#define POW_MEASURE_NUM 10
#define POW_MEASURE_INTERVAL 3000
#define POW_CONV_INTERVAL 10


#define ADC_DIVIDER_VAL 1
#define ADC_REF_VOLTAGE 0.6
#define ADC_MAX_VALUE 0x0FFF
#define ADC_DIV 4

#define ADC_VOLTAGE_PER_ADC_VAL (((ADC_REF_VOLTAGE * ADC_DIV)/ADC_MAX_VALUE)*ADC_DIVIDER_VAL)


void saadc_callback(nrf_drv_saadc_evt_t const * p_event);
void adc_task_function (void * pvParameter);	
#endif
