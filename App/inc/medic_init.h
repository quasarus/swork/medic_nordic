#ifndef __MEDIC_INIT
#define __MEDIC_INIT

#define L1_COM_PIN 10
#define L2_COM_PIN 11
#define L3_COM_PIN 12
#define L4_COM_PIN 13

#define LED_R_PIN 16
#define LED_G_PIN 15
#define LED_B_PIN 14

#define SOUND_PIN 18

#define BTN1_PIN 17
#define BTN2_PIN 19

#define EXT_PWR_PIN 23

#define IR_TX_PIN 21

#define CHRG_PIN 22

#define BAT_AIN_PIN 5

#define EEPROM_SCK_PIN 7
#define EEPROM_MISO_PIN 8
#define EEPROM_MOSI_PIN 9
#define EEPROM_CS_PIN 6

#define OLED_POW_DIS_PIN 4

#define OLED_SCK_PIN 2
#define OLED_MOSI_PIN 3
#define OLED_CS_PIN 0

#define OLED_DC_PIN 1

#define OLED_RES_PIN 25

#define OLED_SPI_INSTANCE 0
#define EEPROM_SPI_INSTANCE 1

void GpioInit(void);

void SpiInit(void);

void TimersInit(void);

void AdcInit(void);
#endif
