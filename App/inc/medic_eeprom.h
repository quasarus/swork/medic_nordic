#ifndef __MEDIC_EEPROM
#define __MEDIC_EEPROM

#include "main.h"

void SpiEepromEventHandle(nrf_drv_spi_evt_t const * p_event,void *p_context);

#endif
