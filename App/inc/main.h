#ifndef __MAIN_H
#define __MAIN_H


#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "timers.h"
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "app_timer.h"
#include "medic_service.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_clock.h"
#include "nrf_drv_spi.h"
#include "nrfx_spim.h"
#include "nrf_drv_spi.h"
#include "nrfx_saadc.h"
#include "nrf_drv_saadc.h"

#include "medic_init.h"
#include "medic_beep.h"
#include "medic_eeprom.h"
#include "medic_evt_ble.h"
#include "medic_lcd.h"
#include "medic_leds.h"
#include "medic_queue.h"
#include "medic_service.h"
#include "medic_adc.h"

#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define DEVICE_NAME                     "Medic"                               /**< Name of device. Will be included in the advertising data. */
#define MEDIC_BASE_UUID_ADV                  {{0x01,0x06,0x00,0x02,0x3F,0x51,0x86,0x0F,0xA8,0x81,0x71,0x45,0x5B,0x8C,0x5B,0xF4}}
#define BLE_UUID_MEDIC_SERVICE_ADV 0x8C5B /**< The UUID of the Nordic UART Service. */
#define MEDIC_COMPANY_ID_ADV 0x000D

#define MEDIC_SERVICE_UUID 0xFFE0
#define MEDIC_CHARACTERISTIC_UUID (MEDIC_SERVICE_UUID)

#define MEDIC_SERVICE_UUID_128                  {{0x01,0x06,0x00,0x02,0x3F,0x51,0x86,0x0F,0xA8,0x81,0x71,0x45,0x5B,0x8C,0x5B,0xF4}}

extern BleTransactDataTypeDef BleTransactData;
uint16_t GetConnectionHandle(void);
#endif

