#include "main.h"

nrf_saadc_value_t AverageAdcResult(nrf_saadc_value_t *convs,uint8_t convNum);
double CalculateVoltage(nrf_saadc_value_t averageAdcVal);

void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
	if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
  {
  }
}



nrf_saadc_value_t AverageAdcResult(nrf_saadc_value_t *convs,uint8_t convNum)
{
	uint32_t sum = 0;
	for(uint8_t cnt = 0; cnt < convNum; cnt++)
	{
		sum += convs[cnt];
	}
	return sum/convNum;
}

double CalculateVoltage(nrf_saadc_value_t averageAdcVal)
{
	return ADC_VOLTAGE_PER_ADC_VAL*(uint16_t)averageAdcVal;
}


nrf_saadc_value_t adc_convs[POW_MEASURE_NUM];

void adc_task_function (void * pvParameter)
{
    UNUSED_PARAMETER(pvParameter);
		BleTransactDataTypeDef *transactData = (BleTransactDataTypeDef*)pvParameter;

    while (true)
    {
			vTaskDelay(POW_MEASURE_INTERVAL);
			for(uint8_t cnt = 0; cnt < POW_MEASURE_NUM; cnt++)
			{
				ret_code_t err_code;
				nrf_saadc_value_t val;
				err_code = nrf_drv_saadc_sample_convert(0, &adc_convs[cnt]);

				vTaskDelay(POW_CONV_INTERVAL);				
			}
			nrf_saadc_value_t avr_adc_val = AverageAdcResult(adc_convs,POW_MEASURE_NUM);
			double voltage = CalculateVoltage(avr_adc_val);
			NRF_LOG_INFO("Voltage is %lf .",voltage);
			
			taskYIELD();
    }
}
