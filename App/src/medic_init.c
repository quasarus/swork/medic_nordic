#include "main.h"


const nrf_drv_spi_t spi_oled = NRF_DRV_SPI_INSTANCE(OLED_SPI_INSTANCE);
const nrf_drv_spi_t spi_eeprom = NRF_DRV_SPI_INSTANCE(EEPROM_SPI_INSTANCE);
void saadc_sampling_event_init(void);
void saadc_sampling_event_enable(void);

void SpiInit(void)
{
	nrf_drv_spi_config_t spi_oled_config = NRF_DRV_SPI_DEFAULT_CONFIG;
	
  spi_oled_config.ss_pin   = OLED_CS_PIN;  
  spi_oled_config.mosi_pin = OLED_MOSI_PIN;
  spi_oled_config.sck_pin  = OLED_SCK_PIN;
	APP_ERROR_CHECK(nrf_drv_spi_init(&spi_oled, &spi_oled_config, SpiOledEventHandle,NULL));
	
	nrf_drv_spi_config_t spi_eeprom_config = NRF_DRV_SPI_DEFAULT_CONFIG;
	
  spi_eeprom_config.ss_pin   = EEPROM_CS_PIN;  
  spi_eeprom_config.mosi_pin = EEPROM_MOSI_PIN;
	spi_eeprom_config.miso_pin  = EEPROM_MISO_PIN;
  spi_eeprom_config.sck_pin  = EEPROM_SCK_PIN;
	APP_ERROR_CHECK(nrf_drv_spi_init(&spi_eeprom, &spi_eeprom_config, SpiEepromEventHandle,NULL));	
}


void GpioInit(void)
{
	
}

void TimersInit(void)
{
	ret_code_t err_code = app_timer_init();
  APP_ERROR_CHECK(err_code);
	
	
}

void AdcInit(void)
{
	
	
	ret_code_t err_code;
  nrf_saadc_channel_config_t channel_config =
  NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN3);

	channel_config.gain = NRF_SAADC_GAIN1_4;
	
  err_code = nrf_drv_saadc_init(NULL, saadc_callback);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_saadc_channel_init(0, &channel_config);
  APP_ERROR_CHECK(err_code);
	
	nrf_saadc_value_t val;
	err_code = nrf_drv_saadc_sample_convert(0, &val);	
  APP_ERROR_CHECK(err_code);
	
}

void saadc_sampling_event_init(void)
{
//    ret_code_t err_code;

//    err_code = nrf_drv_ppi_init();
//    APP_ERROR_CHECK(err_code);

//    nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
//    timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_32;
//    err_code = nrf_drv_timer_init(&m_timer, &timer_cfg, timer_handler);
//    APP_ERROR_CHECK(err_code);

//    /* setup m_timer for compare event every 400ms */
//    uint32_t ticks = nrf_drv_timer_ms_to_ticks(&m_timer, 400);
//    nrf_drv_timer_extended_compare(&m_timer,
//                                   NRF_TIMER_CC_CHANNEL0,
//                                   ticks,
//                                   NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK,
//                                   false);
//    nrf_drv_timer_enable(&m_timer);

//    uint32_t timer_compare_event_addr = nrf_drv_timer_compare_event_address_get(&m_timer,
//                                                                                NRF_TIMER_CC_CHANNEL0);
//    uint32_t saadc_sample_task_addr   = nrf_drv_saadc_sample_task_get();

//    /* setup ppi channel so that timer compare event is triggering sample task in SAADC */
//    err_code = nrf_drv_ppi_channel_alloc(&m_ppi_channel);
//    APP_ERROR_CHECK(err_code);

//    err_code = nrf_drv_ppi_channel_assign(m_ppi_channel,
//                                          timer_compare_event_addr,
//                                          saadc_sample_task_addr);
//    APP_ERROR_CHECK(err_code);
}


void saadc_sampling_event_enable(void)
{
//    ret_code_t err_code = nrf_drv_ppi_channel_enable(m_ppi_channel);

//    APP_ERROR_CHECK(err_code);
}
