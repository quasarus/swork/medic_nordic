#include "main.h"


static uint8_t RxDataQueueBuf[QUEUE_RX_DATA_SIZE];
static uint8_t TxDataQueueBuf[QUEUE_TX_DATA_SIZE];

static uint32_t RxDataQueueBufOffset = 0;
static uint32_t TxDataQueueBufOffset = 0;

//static uint8_t *pRxDataQueueBufEnd = &RxDataQueueBuf[0] + QUEUE_RX_DATA_SIZE;
//static uint8_t *pTxDataQueueBufEnd = &TxDataQueueBuf[0] + QUEUE_TX_DATA_SIZE;
#if defined (UART_PRESENT)
static TaskHandle_t  ble_dbg_task_handle;
static void dbg_task_function (void * pvParameter);
#endif


static TaskHandle_t  ble_tx_task_handle;
static TaskHandle_t ble_rx_task_handle;
static TaskHandle_t main_task_handle;
static TaskHandle_t adc_task_handle;
static void ble_rx_task_function (void * pvParameter);
static void ble_tx_task_function (void * pvParameter);
static void main_task_function (void * pvParameter);
static int RxQueueElementHandler(uint8_t * pItem, uint32_t itemLen);
static int TxQueueElementHandler(uint8_t * pItem, uint32_t itemLen);

void InitOs(void)
{
	
#if defined (UART_PRESENT)
	if(xTaskCreate(main_task_function, "DBG", configMINIMAL_STACK_SIZE, NULL, 1, &main_task_handle) != pdPASS)
	{
		NRF_LOG_ERROR("Can not create task");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

#endif
	if(xTaskCreate(adc_task_function, "adc", configMINIMAL_STACK_SIZE, (void*)&BleTransactData, 1, &adc_task_handle) != pdPASS)
	{
		NRF_LOG_ERROR("Can not create task");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	if(xTaskCreate(main_task_function, "main", configMINIMAL_STACK_SIZE + 300, (void*)&BleTransactData, 1, &main_task_handle) != pdPASS)
	{
		NRF_LOG_ERROR("Can not create task");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	if(xTaskCreate(ble_tx_task_function, "BLE1", configMINIMAL_STACK_SIZE + 300, (void*)&BleTransactData, 1, &ble_tx_task_handle) != pdPASS)
	{
		NRF_LOG_ERROR("Can not create task");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	if(xTaskCreate(ble_rx_task_function, "BLE2", configMINIMAL_STACK_SIZE + 300, (void*)&BleTransactData, 1, &ble_rx_task_handle) != pdPASS)
	{
		NRF_LOG_ERROR("Can not create task");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	
	BleTransactData.RxQueue = xQueueCreate(QUEUE_RX_MAX_LEN, sizeof(BleQueueItemTypeDef));
	BleTransactData.TxQueue = xQueueCreate(QUEUE_TX_MAX_LEN, sizeof(BleQueueItemTypeDef));	
	
}

//���������� �������� �������� �������
static int RxQueueElementHandler(uint8_t * pItem, uint32_t itemLen)
{
	return 0;
}


//���������� �������� ���������� �������
static int TxQueueElementHandler(uint8_t * pItem, uint32_t itemLen)
{						
		uint16_t length = (uint16_t)itemLen;
		
		return ble_medic_data_send(BleTransactData.pMedicInstance,pItem,&length,GetConnectionHandle());			
}

#if defined (UART_PRESENT)
//������� ������� ���-������
static void dbg_task_function (void * pvParameter)
{
    UNUSED_PARAMETER(pvParameter);
    while (true)
    {
			NRF_LOG_FLUSH();				
			taskYIELD();
    }
}
#endif

//������� ��������� �������� ������
static void main_task_function (void * pvParameter)
{
    UNUSED_PARAMETER(pvParameter);
		BleTransactDataTypeDef *transactData = (BleTransactDataTypeDef*)pvParameter;
    while (true)
    {
			vTaskDelay(3000);
			uint8_t dt[]= {0x01,0x02,0x03};
			PutDataToTxQueue(dt,3);
			taskYIELD();			
    }
}

//������� ��������� ������ ������
static void ble_rx_task_function (void * pvParameter)
{
    UNUSED_PARAMETER(pvParameter);
		BleTransactDataTypeDef *transactData = (BleTransactDataTypeDef*)pvParameter;
    while (true)
    {
			
			if(uxQueueMessagesWaiting(transactData->RxQueue) > 0)								//���� � ������� ������ ���� ��������
			{
				GetDataFromRxQueue(RxQueueElementHandler);
			}
			taskYIELD();      
    }
}

//������� ��������� ������ ��������
static void ble_tx_task_function (void * pvParameter)
{
    UNUSED_PARAMETER(pvParameter);
		BleTransactDataTypeDef *transactData = (BleTransactDataTypeDef*)pvParameter;
	
    while (true)
    {	
						
			if(uxQueueMessagesWaiting(transactData->TxQueue) > 0)								//���� � ������� ������ ���� ��������
			{				
				GetDataFromTxQueue(TxQueueElementHandler);				
			}
			taskYIELD();  
    }
}

//���������� ������ � ������� ������ �� BLE
BaseType_t PutDataToRxQueue(const uint8_t *data,uint16_t len)
{
	BaseType_t result = pdFAIL;																																//��������� ���������� ���������� ������ � �������
	uint8_t *pCurrentData;																																		//��������� �� ������ � ������ ������ �������
	BleQueueItemTypeDef queueItem;																														//������� �������	
	
	//���������� �������� ������ � ������ ������ ������� � ��������� �� ������ � ������ ������ �������
	uint32_t offset = (RxDataQueueBufOffset > QUEUE_RX_DATA_SIZE) ? 0 : RxDataQueueBufOffset;	
	pCurrentData = RxDataQueueBuf + offset;
	
	//���������� ��������� �� ������ �������� ������� � ������ � �����
	queueItem.pItem = pCurrentData;
	queueItem.ItemSize = len;
	
	
	result = xQueueSend(BleTransactData.RxQueue,&queueItem,0);																//��������� ������ � �������
	if(result != pdPASS)																																			//���� ������� ��������� - ������� ������																												
	{
		return result;
	}
	
	memcpy(pCurrentData,data,len); 																														//���� � ������� ���� ����� - ����������� ������ � ����� ������ �������
	RxDataQueueBufOffset = offset + len;																											//��������� ��������	
	return result;		
}

//���������� ������ � ������� �������� �� BLE (���������� �-��� PutDataToTxQueue)
BaseType_t PutDataToTxQueue(const uint8_t *data,uint16_t len)
{
	BaseType_t result = pdFAIL;																																
	uint8_t *pCurrentData;																																		
	BleQueueItemTypeDef queueItem;																															
	
	
	uint32_t offset = (TxDataQueueBufOffset > QUEUE_TX_DATA_SIZE) ? 0 : TxDataQueueBufOffset;	
	pCurrentData = TxDataQueueBuf + offset;
	
	
	queueItem.pItem = pCurrentData;
	queueItem.ItemSize = len;
	
	
	result = xQueueSend(BleTransactData.TxQueue,&queueItem,0);																										
	if(result != pdPASS)																																																															
	{
		return result;
	}
	
	memcpy(pCurrentData,data,len); 																														
	TxDataQueueBufOffset = offset + len;																												
	return result;		
}

//��������� ������ �� ������� ������ �� BLE
BaseType_t GetDataFromRxQueue(QueueItemHandlerTypeDef itemHandleCallBack)
{
	BaseType_t result = pdFAIL;
	
  if(itemHandleCallBack == NULL)																						//�������� ������� �-��� ��������� ������
	{
		return result;
	}
	
	BleQueueItemTypeDef queueItem;																						//��-� �������
	result = xQueuePeek(BleTransactData.RxQueue,&queueItem,1);								//��������� �������� ������� ��� �������� �� �������
	if(result == pdPASS)																											//���� ��-� ������� �������
	{					
		if(itemHandleCallBack(queueItem.pItem,queueItem.ItemSize) == 0)				//����� �-��� ��������� ������ ��� ���������
		{
			result = xQueueReceive(BleTransactData.RxQueue,&queueItem,1);				//��������� �������� ������� � ��� ��������� �� ������� ��� �������� ���������� �������� � callback
		}		
	}
	
	return result;		
}

uint8_t cnnn = 0;
//��������� ������ �� ������� �������� �� BLE
BaseType_t GetDataFromTxQueue(QueueItemHandlerTypeDef itemHandleCallBack)
{
	BaseType_t result = pdFAIL;
	
  if(itemHandleCallBack == NULL)																						//�������� ������� �-��� ��������� ������
	{
		return result;
	}
	
	BleQueueItemTypeDef queueItem;																						//��-� �������
	result = xQueuePeek(BleTransactData.TxQueue,&queueItem,5000);								//��������� �������� ������� ��� �������� �� �������
	if(result == pdPASS)																											//���� ��-� ������� �������
	{					
		if(itemHandleCallBack(queueItem.pItem,queueItem.ItemSize) == 0)				//����� �-��� ��������� ������ ��� ���������
		{
			
			result = xQueueReceive(BleTransactData.TxQueue,&queueItem,5000);				//��������� �������� ������� � ��� ��������� �� ������� ��� �������� ���������� �������� � callback
						
		}	
				
	}
	
	return result;		
}

